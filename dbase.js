require("dotenv").config()
const mysql = require('mysql')
const {promisify} = require('es6-promisify')
const db = mysql.createConnection({
    host     : process.env.MYSQL_HOST,
    user     : process.env.MYSQL_USER,
    password : process.env.MYSQL_PASSWORD,
    database : process.env.MYSQL_DB,
  });

db.connect(function(err) {
    if (err) throw err;
    console.log("Database connected...");
});

exports.db = db
exports.dbQueryPromisified = promisify(db.query.bind(db))


