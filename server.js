const express = require("express")
const app = express()
const {catchExprErrors} = require('./handlers')
const {dbQueryPromisified} = require('./dbase')

app.use(express.static("public"))

app.get("/", function(req, res) {
  res.sendFile(__dirname + "/views/index.html");
})

app.get("/getData", catchExprErrors(async(req, res) => {
  const datesPromise = dbQueryPromisified('SELECT * FROM dates ORDER BY date ASC')  
  const usersPromise = dbQueryPromisified('SELECT * FROM users ORDER BY id ASC')
  const [dates, users] = await Promise.all([datesPromise, usersPromise])
  res.json({dates, users})
}))

const listener = app.listen(process.env.PORT, function() {
  console.log("Your app is listening on port " + listener.address().port);
});
