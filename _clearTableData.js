const {catchAsyncErrors} = require('./handlers')
const {dbQueryPromisified} = require('./dbase')

catchAsyncErrors(async () =>{
    await dbQueryPromisified("TRUNCATE TABLE users")
    await dbQueryPromisified("TRUNCATE TABLE dates")
    console.log('Tables were cleared...')
  })()