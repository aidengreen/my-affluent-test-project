require("dotenv").config()
const {catchAsyncErrors} = require('./handlers')
const {dbQueryPromisified} = require('./dbase')
const puppeteer = require('puppeteer')


catchAsyncErrors(async () => {
  const browser = await puppeteer.launch({/* headless: false */ });
  const page = await browser.newPage();
  await page.goto('https://publisher-dev.affluent.io/login', {waitUntil:'networkidle2'});

  // logging into the system
  await page.waitForSelector('.form-control[type=email]');
  await page.focus('.login-form .form-control[type=email]');
  await page.keyboard.type(process.env.WEB_USER);
  await page.focus('.login-form .form-control[type=password]');
  await page.keyboard.type(process.env.WEB_USER_PASSWORD);
  await page.click('.login-form .form-actions button[type=submit]');

  // going to Dates Overview page
  await page.waitForSelector('.home-table-fullreport[href="list?type=dates"]');
  await Promise.all([
    page.waitForNavigation({waitUntil: 'networkidle2'}),
    page.click('.home-table-fullreport[href="list?type=dates"]'),
  ]);

  // setting up daterangepicker
  await page.waitForSelector('#dashboard-report-range')
  await page.evaluate(() => { 
    document.querySelector('#dashboard-report-range').click() 
    document.querySelector('.input-mini[name="daterangepicker_start"]').value = ''
    document.querySelector('.input-mini[name="daterangepicker_end"]').value = ''
  });
  await page.focus('.input-mini[name="daterangepicker_start"]');
  await page.keyboard.type('08/01/2019');
  await page.focus('.input-mini[name="daterangepicker_end"]');
  await page.keyboard.type('08/31/2019');
  await Promise.all([
    page.waitForNavigation({waitUntil: 'networkidle2'}),
    page.click('.range_inputs .applyBtn'),
  ]);

  await page.waitFor(() => document.querySelector('.page-content-body table[data-url="dates"] tbody tr a'))

  let totalDates = []
  let pageDates
  let onLastPage = false
 
  
  while(onLastPage == false){
    
    pageDates = await page.evaluate(() => { 
      let rowsRaw =  [...document.querySelectorAll('.page-content-body table[data-url="dates"] tbody tr')]
      return rowsRaw.map(row => {
        return [...row.querySelectorAll('td')].map((td, index) => (index == 0)? td.childNodes && td.childNodes[0].getAttribute('href'):td.innerText)})
        .map(row => row.map((data, index) => (index == 0) ? data.replace(/.*(\d\d\d\d-\d\d-\d\d).*/gi,'$1') : data.replace(/[$%,]/gi,'').trim()) // formating data
        ) 
        .map((data) =>{
          const [date, commissionsTotal, salesNet, leadsNet, clicks, epc, impressions, cr] = [...data]
          return {date, commissionsTotal, salesNet, leadsNet, clicks, epc, impressions, cr}
        })
    });


    onLastPage = await page.evaluate(() => document.querySelector('.pagination .next.disabled') ? true : false);
    if(!onLastPage){
      await page.evaluate(() => document.querySelector('.pagination .next a').click());
      
      // using load-spinner as an indicator that dataloading is complete 
      await page.waitFor(() => document.querySelector('.affLoadSpinner')) // spinner appeared on the page - loading began
      await page.waitFor(() => !document.querySelector('.affLoadSpinner')) // spinner removed from the page - new data loaded into the DOM, everything ready for the next turn
    }

    totalDates = [...totalDates, ...pageDates]
  }  
  
  await dbQueryPromisified('INSERT INTO dates(date, commissionsTotal, salesNet, leadsNet, clicks, epc, impressions, cr) VALUES ?', [totalDates.map(row => [row.date, row.commissionsTotal, row.salesNet, row.leadsNet, row.clicks, row.epc, row.impressions, row.cr])])
  console.log('The dates were successfully added to the database...')
  await browser.close();
})()