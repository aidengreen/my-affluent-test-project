const axios = require('axios')
const {catchAsyncErrors} = require('./handlers')
const {dbQueryPromisified} = require('./dbase')

catchAsyncErrors(async () =>{
  let {data: {total_pages, data:totalUsers}}  = await axios('https://reqres.in/api/users')

  if(total_pages > 1){ //if where is more then just one page
    const restPagesLinks = []

    for(let i = 2; i <= total_pages; i++){
      restPagesLinks.push(`https://reqres.in/api/users?page=${i}`)
    }

    const restPagesRaw = await Promise.all(restPagesLinks.map(axios))
    const restUsers = restPagesRaw
        .map(({data}) => data)
        .reduce((sumUsers, {data}) => {
          sumUsers = [...sumUsers, ...data]
          return sumUsers
        }, [])

    totalUsers = [...totalUsers, ...restUsers]
  }

  await dbQueryPromisified('INSERT INTO users(id, email, first_name, last_name, avatar) VALUES ?',[totalUsers.map(row => [row.id, row.email, row.first_name, row.last_name, row.avatar])])
  console.log('The users were successfully added to the database...')
})()


    