// catching errors in async-await functions in Express.js 
exports.catchExprErrors = (fn) => {
  return (req, res, next) => fn(req, res, next).catch(next)
}

// catching errors in regular Async function (not Express.js)
exports.catchAsyncErrors = (fn) => {
  return (...params) => fn(...params).catch(error => console.warn(error))
}

